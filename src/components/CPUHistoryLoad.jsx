import React, { useEffect, useState } from "react";
import Chart from "chart.js/auto";
import 'chartjs-adapter-date-fns';

import {buildInitialCategories, buildInitialCPUData, pushNewCategory, pushNewCPUData} from "../logic/cpu";
import {socket, websocketdata} from "../logic/cpusocket";

const DATABUFFERSIZE = 100;

export const CPUHistoryLoad = (props) => {
  const ctxref = React.createRef();

  const drawChartJS = (cpucoresusagechart) => {
    console.log(websocketdata);
    if (cpucoresusagechart.data.datasets[0] === undefined ) {
      cpucoresusagechart.data.datasets = initialDatasets();
      return;
    }
    for (let i = 0; i < websocketdata.ncores; i++) {
      cpucoresusagechart.data.datasets[i].data = pushNewCPUData(cpucoresusagechart.data.datasets[i].data, websocketdata.cores_usage[i]);
    }
    cpucoresusagechart.data.labels = pushNewCategory(cpucoresusagechart.data.labels, new Date().toISOString());
    cpucoresusagechart.update("none"); // none to avoid an animation
    cpucoresusagechart.resize(); // resize the chart to fit the canvas
  }

  const initialDatasets = () => {
    const dsets = []
    for (let i = 0; i < websocketdata.ncores; i++) {
      dsets.push({
        fill: false,
        label: `core ${i}`,
        data: buildInitialCPUData(DATABUFFERSIZE),
        pointRadius: 0
      })
    }
    return dsets
  }

  useEffect(() => {
    const ctx = ctxref.current;
    let frameCount = 0
    let animationFrameId
    let previousTimeStamp

    const cpucoresusagechart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: buildInitialCategories(DATABUFFERSIZE),
          datasets: initialDatasets()
        },
        options: {
          responsive: true,
          events: [
            'click'
          ],
          plugins: {
            title: {
              display: true,
              text: 'CPU Usage History',
            },
            tooltip: {
              enabled: false
            },
            legend: {
              display: true,
                position: 'right',
                align: 'center',
                labels: {
                  usePointStyle: true,
                },
            }
          },
          animation: {
            duration: false
          },
          scales: {
            y: {
              display: true,
              beginAtZero: true,
              suggestedMax: 100,
              suggestedMin: 0,
              ticks: {
                stepSize: 25,
                callback: function(value, index, values) {
                  return value + '%';
                }
              },
            },
            x: {
              display: true,
              type: 'timeseries',
              ticks: {
                source: 'labels',
                maxTicksLimit: 10
              },
              time: {
                unit: 'second',
                displayFormats: {
                  second: 'HH:mm:ss'
                }
              }
            }
          }
        }
      });

    const renderChart = (domHighResTimeStamp) => {
      frameCount++
      if (frameCount) {
        if (previousTimeStamp === undefined) {
          previousTimeStamp = domHighResTimeStamp
        }
        const elapsed = domHighResTimeStamp - previousTimeStamp
        if (elapsed >= 1) {
          drawChartJS(cpucoresusagechart)
          previousTimeStamp = domHighResTimeStamp
        }
        animationFrameId = window.requestAnimationFrame(renderChart)
    }}
    renderChart()

    return () => {
      cpucoresusagechart.destroy();
      window.cancelAnimationFrame(animationFrameId)
    }
  }, [drawChartJS]);

  return (
    <div>
      <canvas ref={ctxref} {...props}/>
    </div>
  );
}