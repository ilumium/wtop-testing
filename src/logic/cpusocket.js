// connect to 127.0.0.1:8080 socket and retrerive data from server
// format is a json object with the following structure:
// { "ncores": 4, "cores_usage": [0, 0, 0, 0], "timestamp": 1707993526}

let socket = new WebSocket("ws://127.0.0.1:8080");
let websocketdata = {};

socket.onopen = function(e) {
  console.log("[open] Connection established");
}

socket.onmessage = function(event) {
  websocketdata = JSON.parse(event.data);
  console.log(`[message] Data received from server: ${event.data}`);
}

socket.onclose = function(event) {
  if (event.wasClean) {
    console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
  } else {
    console.log('[close] Connection died');
  }
}

socket.onerror = function(error) {
  console.log(`[error] ${error.message}`);
}

export {socket, websocketdata};