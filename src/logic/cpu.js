const buildInitialCategories = (size) => {
  const categories = [];
  const now = new Date();
  for (let i = 0; i < size; i++) {
    const time = new Date(now.getTime() - i * 1000);
    categories.unshift(time.toISOString());
  }
  return categories;
}

const pushNewCategory = (categories, value) => {
  return [...categories, value].slice(1);
}

const buildInitialCPUData = (size) => {
  return Array(size).fill(0);
}

const pushNewCPUData = (data, value) => {
  return [...data, value].slice(1);
}

module.exports = {
  buildInitialCategories,
  pushNewCategory,
  buildInitialCPUData,
  pushNewCPUData
}