import { buildInitialCategories, pushNewCategory, buildInitialCPUData, pushNewCPUData } from './cpu';

const SIZE = 5;
describe('CPU logic', () => {

  it('should build initial categories', () => {
    const categories = buildInitialCategories(SIZE);
    expect(categories.length).toBe(SIZE);
  });

  it('should push new category', () => {
    const categories = buildInitialCategories(SIZE);
    const newCategories = pushNewCategory(categories);
    expect(newCategories.length).toBe(SIZE);
  });

  it('should build initial CPU data', () => {
    const data = buildInitialCPUData(SIZE);
    expect(data.length).toBe(SIZE);
  });

  it('should push new CPU data', () => {
    let data = buildInitialCPUData(SIZE);
    for (let i = 0; i < SIZE; i++) {
      data = pushNewCPUData(data, i);
      expect(data.length).toBe(SIZE);
      expect(data[4]).toBe(i);
    }
    for (let i = 0; i < SIZE; i++) {
      expect(data[i]).toBe(i);
    }
  });
  
});